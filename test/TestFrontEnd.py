import unittest
from unittest.mock import MagicMock

from epydemic.epydemic import CONST
from epydemic.main import main, Response, Action


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.by = MagicMock(name="browser")
        Response.add = MagicMock(name="add")
        self.epydemic, self.render = main(self.by)
        self.action = None

    def _render(self):
        self.action = self.render()
        return self.action

    def test_initial_classes(self):
        """check Epydemic and Action"""
        self.assertEqual(self.epydemic.num_epidemic_cards, 4)
        self.assertEqual(self._render().city, "atlanta")

    def test_initial_infection(self):
        """check Action infect 9 cities"""
        self.assertEqual(self._render().city, "atlanta")
        infection_cities = list(self.epydemic.cities.values())
        self.assertEqual(len(infection_cities), 48,  f"not 48 but {len(infection_cities)}")
        infection_cubes = []
        [infection_cubes.extend(cubes) for city in infection_cities for cubes in list(city.cubes.values()) if cubes]
        self.assertEqual(9+9,  len(infection_cubes), f"not 18 but {infection_cubes}")
        return

    def test_can_cure_disease(self):
        """check Response Cure Render"""
        self.assertEqual(self._render().city, "atlanta")
        self.assertEqual(self.epydemic.turn.cure_need, 3)
        self.assertIsNotNone(can := self.action.RESPONSE.cure.render(), f"but was {can}")
        event = MagicMock(name="event")
        # event.target = MagicMock(name="target")
        event.target.id = "_red_"
        can = self.action.RESPONSE.cure._action(event)
        self.assertIsNotNone(can, f"but was {can}")

    def test_cure_a_disease(self):
        """check Response Cure Render"""
        self.assertEqual(self._render().city, "atlanta")
        self.epydemic.turn.cure_need = 2
        self.assertEqual(self.epydemic.turn.cure_need, 2)
        self.assertIsNotNone(can := self.action.RESPONSE.cure.render(), f"but was {can}")
        event = MagicMock(name="event")
        cure_kind = self.epydemic.can_cure()[0]
        self.assertIn(cure_kind, CONST.DISEASES, f"but was {cure_kind}")
        event.target.id = f"_{cure_kind}_"
        self.action.ACTION.cure.action(event)
        cured = self.epydemic.infection_turn.cured
        self.assertIsNotNone(cured, f"@@@@@@@@@@@@@>>>>> but was {cured}")
        self.assertIn(cure_kind, cured, f"but {cure_kind} not in {cured}")
        all_diseases = self.epydemic.all_diseases()
        all_diseases_count = sum(len(diseases) for diseases in all_diseases.values())
        self.assertEqual(18, all_diseases_count, f"but {all_diseases}")

    def test_treat_a_disease(self):
        """check Response Treat Render"""
        self.assertEqual(self._render().city, "atlanta")
        all_diseases = self.epydemic.all_diseases()
        all_diseases_count = sum(len(diseases) for diseases in all_diseases.values())
        self.assertEqual(18, all_diseases_count, f"but {all_diseases}")
        first_disease = all_diseases[list(all_diseases)[0]][0]
        disease_kind = first_disease.split("_")[0]
        self.assertIn(disease_kind, CONST.DISEASES, f"but was {disease_kind}")
        disease_list = Action.RESPONSE.infect.PAINT[disease_kind].counters
        self.assertIn(first_disease, disease_list,disease_list)
        disease_marker = disease_list[first_disease]
        self.assertIsNotNone(disease_marker, disease_marker)
        disease_list[first_disease] = MagicMock(name=first_disease)
        event = MagicMock(name="event")
        event.target.id = f"_{first_disease}"
        kind, _ = first_disease.split("_")
        city = [city_.name for city_ in self.epydemic.cities.values()
                for dkinds in city_.cubes.values() for mark in dkinds if mark == first_disease][0]
        self.epydemic.turn.city = city
        event.target.dataset.city = city
        self.assertEqual(city, self.epydemic.current_city().name)
        result = Action.ACTION.player.action(event)
        self.assertIn(first_disease, result,f"ActionTreat refused: {result} city: {city}")


if __name__ == '__main__':
    unittest.main()
