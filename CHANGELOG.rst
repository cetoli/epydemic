Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_.


`Unreleased`_
-------------

Added
+++++
- expected look only version.


`20.04`_ 
--------

Fixed
++++++++
* remove index not used in exeption.
* extract action as a function since not working as static.

Added
+++++
- original files from `pydemic`_ by `tomaskeefe`_.
- original assets from `pandemic`_ by `alexzherdev`_.

-------

Laboratório de Automação de sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**


.. ..

 [LABASE](http://labase.sefip.org) -
 [Instituto Tércio Pacitti](http://nce.ufrj.br) -
 [UFRJ](http://www.ufrj.br)

 ![LABASE Laboratório de Automação de sistemas Educacionais][logo]

 [logo]: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png "Laboratório de Automação de sistemas Educacionais"
 <!---

.. ..

LABASE_ - `Instituto Tércio Pacitti`_ - UFRJ_

.. image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :alt: Laboratório de Automação de sistemas Educacionais


.. _LABASE: http://labase.nce.ufrj.br/
.. _Instituto Tércio Pacitti: http://www.nce.ufrj.br/
.. _ufrj: http://www.UFRJ.BR/
.. _Unreleased: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _20.04: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _20.04.1: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _pydemic: https://github.com/thomaskeefe/pydemic
.. _thomaskeefe: https://github.com/thomaskeefe
.. _alexzherdev: https://github.com/alexzherdev
.. _pandemic: https://github.com/alexzherdev/pandemic

.. ..

 --->

.. ..