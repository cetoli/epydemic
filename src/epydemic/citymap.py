# -*- coding: UTF8 -*-
# This file is part of  program Epydemic
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception
CM = {
    "san_francisco": {
        "name": "san_francisco",
        "paths": [
            "chicago",
            "los_angeles",
            "manila",
            "tokyo"
        ],
        "color": "blue",
        "y": "187.78378",
        "x": "99.993591",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "chicago": {
        "name": "chicago",
        "paths": [
            "montreal",
            "atlanta",
            "mexico_city",
            "los_angeles",
            "san_francisco"
        ],
        "color": "blue",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "129.81592",
        "y": "174.9704"
    },
    "montreal": {
        "name": "montreal",
        "paths": [
            "chicago",
            "new_york",
            "washington"
        ],
        "color": "blue",
        "y": "170.58586",
        "x": "154.61115",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "new_york": {
        "name": "new_york",
        "paths": [
            "london",
            "madrid",
            "washington",
            "montreal"
        ],
        "color": "blue",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "171.54448",
        "y": "175.87753"
    },
    "washington": {
        "name": "washington",
        "paths": [
            "montreal",
            "new_york",
            "atlanta",
            "miami"
        ],
        "color": "blue",
        "y": "189.10675",
        "x": "163.07777",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "atlanta": {
        "name": "atlanta",
        "paths": [
            "washington",
            "miami",
            "chicago"
        ],
        "color": "blue",
        "y": "192.7433",
        "x": "130.92673",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "london": {
        "name": "london",
        "paths": [
            "essen",
            "paris",
            "madrid",
            "new_york"
        ],
        "color": "blue",
        "y": "167.91675",
        "x": "221.53078",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "madrid": {
        "name": "madrid",
        "paths": [
            "london",
            "paris",
            "algiers",
            "sao_paolo",
            "new_york"
        ],
        "color": "blue",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "220.41995",
        "y": "185.31938"
    },
    "paris": {
        "name": "paris",
        "paths": [
            "essen",
            "milan",
            "algiers",
            "madrid",
            "london"
        ],
        "color": "blue",
        "y": "175.87753",
        "x": "236.15637",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "essen": {
        "name": "essen",
        "paths": [
            "st_petersburg",
            "milan",
            "paris",
            "london"
        ],
        "color": "blue",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "244.85771",
        "y": "162.54785"
    },
    "milan": {
        "name": "milan",
        "paths": [
            "essen",
            "istanbul",
            "paris"
        ],
        "color": "blue",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "251.70767",
        "y": "178.46942"
    },
    "st_petersburg": {
        "name": "st_petersburg",
        "paths": [
            "moscow",
            "istanbul",
            "essen"
        ],
        "color": "blue",
        "y": "159.0303",
        "x": "266.14816",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "los_angeles": {
        "name": "los_angeles",
        "paths": [
            "chicago",
            "mexico_city",
            "sydney",
            "san_francisco"
        ],
        "color": "yellow",
        "y": "206.49359",
        "x": "101.88331",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "mexico_city": {
        "name": "mexico_city",
        "paths": [
            "chicago",
            "miami",
            "bogota",
            "lima",
            "los_angeles"
        ],
        "color": "yellow",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "125.76565",
        "y": "213.15843"
    },
    "miami": {
        "name": "miami",
        "paths": [
            "washington",
            "bogota",
            "mexico_city",
            "atlanta"
        ],
        "color": "yellow",
        "y": "203.71658",
        "x": "157.97903",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "bogota": {
        "name": "bogota",
        "paths": [
            "miami",
            "sao_paolo",
            "buenos_aires",
            "lima",
            "mexico_city"
        ],
        "color": "yellow",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "156.68309",
        "y": "225.00705"
    },
    "lima": {
        "name": "lima",
        "paths": [
            "bogota",
            "santiago",
            "mexico_city"
        ],
        "color": "yellow",
        "y": "238.89214",
        "x": "143.16827",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "santiago": {
        "name": "santiago",
        "paths": [
            "lima"
        ],
        "color": "yellow",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "156.49796",
        "y": "256.47992"
    },
    "sao_paolo": {
        "name": "sao_paolo",
        "paths": [
            "madrid",
            "lagos",
            "buenos_aires",
            "bogota"
        ],
        "color": "yellow",
        "y": "256.47992",
        "x": "185.56406",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "buenos_aires": {
        "name": "buenos_aires",
        "paths": [
            "sao_paolo",
            "bogota"
        ],
        "color": "yellow",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "174.08572",
        "y": "269.8096"
    },
    "lagos": {
        "name": "lagos",
        "paths": [
            "khartoum",
            "kinshasa",
            "sao_paolo"
        ],
        "color": "yellow",
        "y": "223.34085",
        "x": "229.25578",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "kinshasa": {
        "name": "kinshasa",
        "paths": [
            "khartoum",
            "johannesburg",
            "lagos"
        ],
        "color": "yellow",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "245.91788",
        "y": "236.4854"
    },
    "khartoum": {
        "name": "khartoum",
        "paths": [
            "johannesburg",
            "kinshasa",
            "lagos",
            "cairo"
        ],
        "color": "yellow",
        "y": "224.82193",
        "x": "267.20834",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "johannesburg": {
        "name": "johannesburg",
        "paths": [
            "khartoum",
            "kinshasa"
        ],
        "color": "yellow",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "260.91379",
        "y": "262.77448"
    },
    "algiers": {
        "name": "algiers",
        "paths": [
            "istanbul",
            "cairo",
            "madrid",
            "paris"
        ],
        "color": "black",
        "y": "194.58736",
        "x": "232.85222",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "istanbul": {
        "name": "istanbul",
        "paths": [
            "st_petersburg",
            "moscow",
            "baghdad",
            "cairo",
            "algiers",
            "milan"
        ],
        "color": "black",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "268.58316",
        "y": "186.62657"
    },
    "cairo": {
        "name": "cairo",
        "paths": [
            "istanbul",
            "baghdad",
            "riyadh",
            "khartoum",
            "algiers"
        ],
        "color": "black",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "264.88046",
        "y": "201.2522"
    },
    "moscow": {
        "name": "moscow",
        "paths": [
            "tehran",
            "istanbul",
            "st_petersburg"
        ],
        "color": "black",
        "y": "174.77797",
        "x": "283.20877",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "baghdad": {
        "name": "baghdad",
        "paths": [
            "tehran",
            "karachi",
            "riyadh",
            "cairo",
            "istanbul"
        ],
        "color": "black",
        "y": "193.47655",
        "x": "281.72769",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "riyadh": {
        "name": "riyadh",
        "paths": [
            "karachi",
            "cairo",
            "baghdad"
        ],
        "color": "black",
        "y": "214.21161",
        "x": "288.57767",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "tehran": {
        "name": "tehran",
        "paths": [
            "delhi",
            "karachi",
            "baghdad",
            "moscow"
        ],
        "color": "black",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "297.83438",
        "y": "186.62657"
    },
    "karachi": {
        "name": "karachi",
        "paths": [
            "delhi",
            "mumbai",
            "riyadh",
            "baghdad",
            "tehran"
        ],
        "color": "black",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "302.09247",
        "y": "203.28868"
    },
    "mumbai": {
        "name": "mumbai",
        "paths": [
            "delhi",
            "chennai",
            "karachi"
        ],
        "color": "black",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "307.46136",
        "y": "216.8035"
    },
    "delhi": {
        "name": "delhi",
        "paths": [
            "kolkata",
            "chennai",
            "mumbai",
            "karachi",
            "tehran"
        ],
        "color": "black",
        "y": "196.99411",
        "x": "317.82889",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "chennai": {
        "name": "chennai",
        "paths": [
            "kolkata",
            "bangkok",
            "jakarta",
            "mumbai",
            "delhi"
        ],
        "color": "black",
        "y": "221.06158",
        "x": "320.23563",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "kolkata": {
        "name": "kolkata",
        "paths": [
            "hong_kong",
            "bangkok",
            "chennai",
            "delhi"
        ],
        "color": "black",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "332.26938",
        "y": "205.14003"
    },
    "bangkok": {
        "name": "bangkok",
        "paths": [
            "hong_kong",
            "ho_chi_minh_city",
            "jakarta",
            "chennai",
            "kolkata"
        ],
        "color": "red",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "340.09726",
        "y": "218.41879"
    },
    "jakarta": {
        "name": "jakarta",
        "paths": [
            "bangkok",
            "ho_chi_minh_city",
            "sydney",
            "chennai"
        ],
        "color": "red",
        "y": "237.48764",
        "x": "335.28375",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "beijing": {
        "name": "beijing",
        "paths": [
            "seoul",
            "shanghai"
        ],
        "color": "red",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "348.05814",
        "y": "179.72568"
    },
    "shanghai": {
        "name": "shanghai",
        "paths": [
            "seoul",
            "tokyo",
            "hong_kong",
            "taipei",
            "beijing"
        ],
        "color": "red",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "354.53781",
        "y": "197.12833"
    },
    "hong_kong": {
        "name": "hong_kong",
        "paths": [
            "taipei",
            "manila",
            "ho_chi_minh_city",
            "bangkok",
            "kolkata",
            "shanghai"
        ],
        "color": "red",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "356.20401",
        "y": "213.04988"
    },
    "ho_chi_minh_city": {
        "name": "ho_chi_minh_city",
        "paths": [
            "hong_kong",
            "manila",
            "jakarta",
            "bangkok"
        ],
        "color": "red",
        "y": "234.15521",
        "x": "358.61072",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "seoul": {
        "name": "seoul",
        "paths": [
            "tokyo",
            "shanghai",
            "beijing"
        ],
        "color": "red",
        "y": "175.83786",
        "x": "367.49725",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "taipei": {
        "name": "taipei",
        "paths": [
            "osaka",
            "manila",
            "hong_kong",
            "shanghai"
        ],
        "color": "red",
        "y": "206.57018",
        "x": "370.6445",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "manila": {
        "name": "manila",
        "paths": [
            "san_francisco",
            "sydney",
            "ho_chi_minh_city",
            "hong_kong",
            "taipei"
        ],
        "color": "red",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "376.75391",
        "y": "228.78632"
    },
    "sydney": {
        "name": "sydney",
        "paths": [
            "los_angeles",
            "jakarta",
            "manila"
        ],
        "color": "red",
        "y": "268.59021",
        "x": "394.15656",
        "height": "8.4666662",
        "width": "8.4666662"
    },
    "tokyo": {
        "name": "tokyo",
        "paths": [
            "san_francisco",
            "osaka",
            "shanghai",
            "seoul"
        ],
        "color": "red",
        "width": "8.4666662",
        "height": "8.4666662",
        "x": "381.01205",
        "y": "181.39189"
    },
    "osaka": {
        "name": "osaka",
        "paths": [
            "taipei",
            "tokyo"
        ],
        "color": "red",
        "y": "195.27698",
        "x": "384.89987",
        "height": "8.4666662",
        "width": "8.4666662"
    }
}