#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Epydemic
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

""" The version counter.

    This module just declares the current version.

No Classes in this module.

Changelog
---------
    20.04.1
        * NEW: version reference.

.. seealso::

   Page :ref:`epydemic_introduction`

"""
__version__ = "20.04.01"
