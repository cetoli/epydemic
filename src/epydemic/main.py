#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program SuperPython
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Generate dynamic site from client side.

    This module controls the creation of the IDE user interface.

Classes in this module:

    :py:class:`Response` Generate dynamic site from use cases.

    :py:class:`CONST` Default file names for methods and other default constants.

Changelog
---------
    *Upcoming*
        * TODO: Improve documentation and test.
        * CHANGE: New Architecture.
        * NEW: ActionCure and ResponseCure
    20.04
        * BETTER: Document classes and methods.
        * CHANGE: Move consts to a class.

.. seealso::

   Page :ref:`epydemic_introduction`

"""
import os
import sys
print(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
try:
    from epydemic.epydemic import Epydemic, CONST
except ModuleNotFoundError as mnf:
    from epydemic import Epydemic, CONST


class Response:
    def __init__(self, browser, game):
        self.game = game
        self.item = None
        self.by, self.dc, self.ht, self.svg = browser, browser.document, browser.html, browser.svg

    def add(self, a="", b=""):
        a = a if a else CONST.LAYER_
        _ = self.dc[a] <= b


class ResponsePlayer(Response):
    def __init__(self, browser, game, player="dispatcher", x=0, y=0):
        super().__init__(browser, game)
        self.player, self.x, self.y = player, x, y
        self.element = self.svg.image(
            href=CONST.PLAYER_PNG.format(self.player), x=x - 2, y=y - 2, id=f"pl{player}", width=10.0, height=10.0)

    def render(self, city):
        # print(f"ResponsePlayer render {city}")
        x, y = self.game.travel(city)
        self.element.setAttribute("x", x - 2)
        self.element.setAttribute("y", y - 2)
        self.add(CONST.LAYER_, self.element)
        # print("self.responses move", self.element, self.dc)

    def init(self):
        self.dc["_splash_"].html = ""
        self.game.start_turn()


class ResponseShowCards(Response):
    def render(self):
        self.__single_initiation(self)
        no_cards = (10 - len(self.game.cards_in_hand())) * [99]
        cards = [CONST.CARDNUM.index(_card) for _card in self.game.cards_in_hand()] + no_cards
        [self.dc[f"_card_slot_{slot + 1}"].setAttribute("src", f"epydemic/images/city-{card}.png")
         for slot, card in enumerate(cards)]
        # turn = self.game.infection_turn.turn()
        turn = self.game.infect()
        print(f"Action.RESPONSE.infect.render() {turn}")
        Action.RESPONSE.infect.render(turn) if turn else None

    @classmethod
    def __single_initiation(cls, self):
        cls.__single_initiation = lambda *_: None

        def _discard(ev):
            self.game.discard(hand_order=int(ev.target.id[11:]))
            # print(type(ev), ev.target.id)
            self.render()
            Action.RESPONSE.move.render()
            Action.RESPONSE.build.render()
        self.dc[f"_hand_"].bind("click", _discard)


class ResponseShowMoves(Response):
    def initiation(self, city, action):
        target = CONST.CROSSHAIR_PNG
        crosshair = self.svg.image(id=f"_target_{city}", href=target, x=-900, y=-900, height=12, width=12)
        self.add(CONST.LAYER_, crosshair)
        crosshair.bind("click", action)

    def reset_targets(self):
        _ = [self.dc[f"_target_{city}"].setAttribute("x", "-900") for city in self.game.cities.keys()]

    def render(self):
        def set_target_location(target):
            self.dc[f"_target_{target.name}"].setAttribute("x", target.x - 2)
            self.dc[f"_target_{target.name}"].setAttribute("y", target.y - 2)
        self.reset_targets()
        # print(f"set_target_location(city) {self.game.destinations()}")
        _ = [set_target_location(city) for city in self.game.destinations()]
        Action.RESPONSE.card.render()
        # self.responses.move.render()
        Action.RESPONSE.cure.render()


class ResponseShowCure(Response):
    HEALER = None
    CURED = {}
    ERADICATED = {}

    def __init__(self, browser, game):
        super().__init__(browser, game)
        self._buider = None
        self.single_initiation() if not ResponseShowCure.HEALER else None

    def single_initiation(self):
        healer_ = CONST.CURE_PNG
        healer = {kind: self.svg.image(
            id=CONST.CITY_CURE_.format(kind), href=healer_.format(kind), x=-900, y=-900, height=8, width=6)
                  for kind in CONST.DISEASES}
        # [self.add(b=heal) or heal.bind("click", lambda ev: None) for heal in healer.values()]
        [self.add(b=heal) or heal.bind("click", lambda ev: Action.ACTION.cure.action(ev)) for heal in healer.values()]
        ResponseShowCure.HEALER = healer
        return healer

    def render(self):
        def post_winning():
            win_image = self.ht.IMG(src=CONST.WIN, width="600px")
            print("ResponseShowCure post winning", win_image)
            self.add(CONST.EPIDEMY, win_image)

        def set_disease_as_erradicated(kind):
            self.CURED[kind].setAttribute("href", CONST.ERADICATED_PNG.format(kind))
            self.ERADICATED[kind] = self.CURED[kind]

        def set_disease_as_cured(kind):
            self.HEALER[kind].setAttribute("x", 55.0)
            self.HEALER[kind].setAttribute("y", 160.0 + CONST.DISEASES.index(kind) * 20.0)
            self.CURED[kind] = self.HEALER.pop(kind)

        def set_target_location(target, kind):
            self.HEALER[kind].setAttribute("x", target.x + 6)
            self.HEALER[kind].setAttribute("y", target.y - 1)
        can_cure = self.game.can_cure()
        set_target_location(self.game.current_city(), can_cure[0]) if can_cure else self.reset()
        all_cured, erradicated = self.game.cured(), self.game.eradicated()
        post_winning() if set(all_cured) == set(CONST.DISEASES) else None
        [set_disease_as_cured(kind) for kind in all_cured if kind not in self.CURED]
        [set_disease_as_erradicated(kind) for kind in erradicated if kind not in self.ERADICATED]
        # print(f"ResponseShowCure can_cure, can_cure,{self.game.current_city().has_research_station}"
        #       f", all_cured, {all_cured}, erradicated, {erradicated}")
        # marker = self.game.infection_turn.markers
        # # [print(f"{kind}: l: {len(marker[kind])} {marker[kind]}") for kind in CONST.DISEASES]

        return can_cure

    def reset(self):
        [healer.setAttribute("x", -900) for healer in self.HEALER.values()]


class ResponseShowBuild(Response):
    BUILDER = None
    SELECTOR = None

    def __init__(self, browser, game):
        super().__init__(browser, game)
        self._buider = None
        self.single_initiation() if not ResponseShowBuild.BUILDER else None

    def single_initiation(self):
        target = CONST.CITY_SELECTION
        build = CONST.STATION_PNG
        selector = self.svg.image(
                id=CONST.CITY_SELECTION_, href=target, x=-900, y=-900, height=13, width=13)
        builder = self.svg.image(id=CONST.CITY_BUILD_, href=build, x=-900, y=-900, height=8, width=6)
        self.add(CONST.LAYER_, builder)
        self.add(CONST.LAYER_, selector)
        builder.bind("click", self._action)
        ResponseShowBuild.BUILDER, ResponseShowBuild.SELECTOR = builder, selector
        return builder, selector

    def _action(self, _):
        print("ResponseShowBuild", self.game.current_city().name)
        ResponseShowBuild.BUILDER.setAttribute("x", -900)
        ResponseShowBuild.SELECTOR.setAttribute("x", -900)
        # noinspection PyCallByClass
        self._buider = ActionBuild(browser=self.by, game=self.game, responses=CONST.Info(0, 0, self, 0, 0, 0))
        self._buider.action()

    def build(self, station):
        x, y = self.game.current_city().build(station)
        self.add(CONST.LAYER_, self.svg.image(
            href=CONST.STATION_PNG, x=x, y=y + 4,
            id=f"station_{station}", width=8.0, height=6.0))
        Action.RESPONSE.move.render()

    def render(self):
        def set_target_location(target=self.game.current_city()):
            self.SELECTOR.setAttribute("x", target.x - 2)
            self.SELECTOR.setAttribute("y", target.y - 2)
            self.BUILDER.setAttribute("x", target.x + 2)
            self.BUILDER.setAttribute("y", target.y - 8)
        set_target_location(self.game.current_city()) if self.game.can_travel else None
        self.game.allow_build(set_target_location, self.reset)
        Action.RESPONSE.move.render()

    def reset(self):
        self.BUILDER.setAttribute("x", -900)
        self.SELECTOR.setAttribute("x", -900)


class ResponseInfect(Response):
    PAINT = None

    def render(self, infection):
        self._single_instance(svg=self.svg, add=self.add)
        [self._render(x, y, city_name, disease, marker) for x, y, city_name, diseases in infection
         for disease, marker in diseases.items() if marker]

    def _render(self, x, y, city_name, disease, marker):
        # print(f"self.PAINT[city_name:{city_name} disease:{disease} at:{x, y}] mark:{marker}")
        self.PAINT[disease].infect(x, y, city_name, marker)
        # [self.PAINT[kind].infect(x, y, counters) for counters in disease_briefing]

    @classmethod
    def _single_instance(cls, svg, add):
        class Infection:
            def __init__(self, kind):
                self.kind = kind
                self.counters = {
                    f"{kind}_{count}": svg.image(
                        href=CONST.VIRUS.format(kind), x=-900, y=-900, id=f"cnt_{kind}_{count}",
                        width=7.0, height=7.0) for count in range(CONST.INFECTION_STOCK)}
                add(b=[counter for counter in self.counters.values()])
                _treat = Action.ACTION.player.action
                [counter.bind("click", _treat) for counter in self.counters.values()]

            def infect_one(self, x=-900, y=-900, city_name="", counter="", d=-2.0):
                # print(f"self.infect_one:{city_name} disease:{counter} at:{x, y}] mark:{self.counters}")
                counter_ = self.counters[counter]
                counter_.setAttribute("x", x+d)
                counter_.setAttribute("y", y-6)
                counter_.setAttribute("data-city", city_name)
                # counter.data("data-city", city_name)

            def infect(self, x, y, city_name, counters):
                d0 = -4*len(counters)/2
                [self.infect_one(x, y, city_name, counter, d0+d*4.0) for d, counter in enumerate(counters)]

            def cure(self, counter):
                self.counters[counter].setAttribute("x", -900)

        cls.PAINT = {kind: Infection(kind) for kind in CONST.DISEASES}
        # print(f"cls.PAINT{cls.PAINT['red'].counters}")
        cls._single_instance = lambda *_, **__: None


class Action(object):
    RESPONSE = None
    ACTION = None

    def __init__(self, city=None, game=None, browser=None, responses=None, actor=None):
        self.city, self.game, self.browser, self.actor = city, game, browser, actor if actor else lambda *_: None
        self.by, self.dc, self.ht, self.svg = browser, browser.document, browser.html, browser.svg
        self.city = city or game.cities[CONST.ATLANTA]
        responses = responses or CONST.INFO
        print("class Action(object):", responses, ResponseInfect(browser=self.browser, game=self.game))
        # noinspection PyCallByClass
        Action.RESPONSE = self.responses = CONST.Info(
            card=responses.card or ResponseShowCards(browser=browser, game=game),
            move=responses.move or ResponseShowMoves(browser=browser, game=game),
            build=responses.build or ResponseShowBuild(browser=browser, game=game),
            player=responses.player or ResponsePlayer(browser=self.browser, game=self.game),
            infect=responses.infect or ResponseInfect(browser=self.browser, game=self.game),
            cure=responses.cure or ResponseShowCure(browser=self.browser, game=self.game)
        )

    def action(self, *_):
        epydemic, brython, response, action = self.game, self.browser, self.RESPONSE, self.ACTION
        epydemic.render()
        Action.ACTION = CONST.Info(
            card=None,
            move=ActionTravel(game=self.game, browser=self.browser),
            build=ActionBuild("atlanta", self.game, browser=self.browser),
            player=ActionTreat(game=self.game, browser=self.browser),
            infect=ActionInfect(game=self.game, browser=self.browser),
            cure=ActionCure(game=self.game, browser=self.browser)
        )
        response, action = self.RESPONSE, self.ACTION
        '''
        ResponseShowCards(browser=self.browser, game=self.game).render()
        ActionBuild("essen", epydemic, browser=brython).build(epydemic.cities["essen"])  # TODO: remove, just for test
        ActionBuild("atlanta", epydemic, browser=brython).build(epydemic.cities["atlanta"])
        print(f"Action ActionBuild{self.browser}")
        ActionTravel(game=epydemic, browser=brython).action()
        turn = self.game.infect(4)
        print(f"Action infect turn {turn}")
        self.responses.infect.render(turn)
        self.responses.player.init()'''
        response.card.render()
        action.build.build(epydemic.cities["essen"])
        action.build.build(epydemic.cities["atlanta"])
        action.move.action()
        # turn = self.game.infect(4)
        turn = self.game.initial_infection()
        print(f"Action self.game.initial_infection(){turn}")
        response.infect.render(turn)
        response.player.init()
        response.cure.render()
        return self


class ActionTravel(Action):
    def __init__(self, city=None, game=None, browser=None, responses=None, master=None):
        super().__init__(city, game, browser, responses)
        # self.responses or CONST.INFO
        self.master = master or self
        if not city:
            [ActionTravel(city=_city, game=game, browser=browser, responses=self.responses, master=self)
             for _city in game.cities.values() if _city.name != CONST.ATLANTA]
        self.responses.move.initiation(city=self.city.name, action=self.action)

    def _action(self, *_):
        # self.responses.card.render()
        self.responses.move.render()
        # self.responses.cure.render()
        build = self.responses.build
        # print(f"_action ResponseShowBuild{build}")
        self.game.allow_build(allow=build.render,refuse=build.reset)

    def action(self, *_):
        # print(f"class ActionTravel action{self.actor}")
        self.responses.player.render(self.city)
        self._action()


class ActionCure(Action):
    def action(self, ev=None):

        ev.target.unbind("click")
        all_cured = self.game.cure()
        print("ResponseShowCure city cure", self.game.current_city().name, all_cured)
        Action.RESPONSE.card.render()
        Action.RESPONSE.cure.render()
        Action.RESPONSE.move.render()
        return all_cured


class ActionTreat(Action):
    def action(self, event=None):
        def reset(disease, counter):
            disease.setAttribute("x", -900)
            return counter

        _, kind, marker = event.target.id.split("_")
        city = event.target.dataset.city
        treat = self.game.treat(city, kind, f"{kind}_{marker}")
        print(f"treat event.target.id {event.target.id} city: {city} treat_list: {treat}")
        treated = [reset(Action.RESPONSE.infect.PAINT[kind].counters[counter], counter)
                   for counter in treat]
        Action.RESPONSE.move.render()
        Action.RESPONSE.card.render()
        return treated


class ActionInfect(Action):
    def action(self, *_):
        cities = self.game.infect()
        print(f"ActionInfect {cities}")
        [self.responses.infect.render(x, y, counters) for x, y, counters in cities]


class ActionBuild(Action):
    def action(self, *_):
        city = self.game.current_city()
        # print("", self.city, city)
        self.game.discard(city.name)
        self.build(city)
        print(f"self.build{city}")

    def build(self, city):
        station = self.game.build(city.name)
        self.responses.card.render()
        self.responses.build.build(station)


def main(brython):
    def _render(*_):
        action_object = Action(browser=brython, game=epydemic, city=CONST.ATLANTA).action()
        epydemic.turn.cure_need = 3
        return action_object

    epydemic = Epydemic()

    brython.timer.set_timeout(_render, 100)
    return epydemic, _render


if __name__ == "__main__":
    def offline_main():
        from unittest.mock import MagicMock

        class Browser:
            mock = MagicMock()
            document, alert, html, window, ajax, timer, svg = [mock] * 7
            Response.add = MagicMock()

        epydemic, render = main(Browser)
        cities = epydemic.render().cities
        print(f"cities: {cities}")
        print(f"cities.keys: {cities.keys()}")
        render()
        action = Action(browser=Browser, game=epydemic, city="atanta").action()
        travel = action.ACTION.move
        # ActionInfect(game=epydemic, browser=Browser).action()
        travel.action()
        travel.action()
        travel.action()
        travel.action()
        travel.action()
        travel.action()
    # _init()
    # _routes()
    offline_main()
