#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Epydemic
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Generate dynamic site from client side.

    This module controls the creation of the IDE user interface.

Classes in this module:

    :py:class:`Epydemic` Main game controller, the *MATCH* use case.

    :py:class:`UCPlayerTurn` Use case for the user turn, use a state machine.

    :py:class:`UCInfectionTurn` Use case for the user turn, use a state machine.

    :py:class:`City` City can hold infections and research centers.

    :py:class:`Util` Scripts to generate html, svg interface and initial city setup.

    :py:class:`CONST` Wrap all constants used in the game.

Changelog
---------
    *Upcoming*
        * TODO: Improve documentation and test.
        * BETTER: Document classes and methods.
        * NEW: Player class to allow more then one player.
        * NEW: Share action in player turn.
    20.04.1
        * NEW: Player and Infection turn use cases.
        * CHANGE: New Architecture, extracted device wrappers into main.
    20.04
        * CHANGE: Move consts to a class.
        * NEW: Use Brython templates.
        * FIX: Working burger menus.

.. seealso::

   Page :ref:`epydemic_introduction`

"""
from collections import namedtuple
try:
    from epydemic.citymap import CM
except ModuleNotFoundError as mnf:
    import citymap
    CM = citymap.CM


def singleton(cls):
    instance = [None]

    def wrapper(*args, **kwargs):
        if instance[0] is None:
            instance[0] = cls(*args, **kwargs)
        return instance[0]

    return wrapper


class Epydemic:
    """ Generate dynamic site from client side"""

    def __init__(self, num_players=1, num_epidemic_cards=4):
        self.info = None
        # self.by, self.dc, self.ht, self.svg = browser, browser.document, browser.html, browser.svg
        self.num_players, self.num_epidemic_cards = num_players, num_epidemic_cards
        # self.turn_count = 0
        self.turn = None
        self.infection_turn = None

    def render(self):
        self.turn = UCPlayerTurn(game=self)
        self.turn.create_cities()
        self.infection_turn = UCInfectionTurn(game=self)
        return self.turn

    def all_diseases(self):
        all_by_kind = {kind: [] for kind in CONST.DISEASES}
        _ = [all_by_kind[kind].extend(city.cubes[kind]) for kind in CONST.DISEASES for city in self.cities.values()]
        return all_by_kind

    def cured(self):
        return list(self.infection_turn.cured)

    def eradicated(self):
        return list(self.infection_turn.eradicated)

    def can_cure(self):
        return self.current_city().has_research_station and self.turn.has_cure()

    def infect(self, rate=0):
        if rate:
            return self.infection_turn.real_turn(rate)
        return self.infection_turn.turn(rate)

    def cards_in_hand(self):
        return self.turn.cards_in_hand

    def city_names(self):
        return self.turn.cities.keys()

    @property
    def can_travel(self):
        return self.turn.turn_state.can_travel

    @property
    def initial_infection(self):
        return self.infection_turn.initial_infection

    @property
    def cities(self):
        return self.turn.cities

    @property
    def build(self):
        return self.turn.build

    @property
    def cure(self):
        return self.turn.cure

    @property
    def current_city(self):
        return self.turn.current_city

    @property
    def treat(self):
        return self.turn.treat

    @property
    def travel(self):
        return self.turn.travel

    @property
    def destinations(self):
        return self.turn.destinations

    @property
    def allow_build(self):
        return self.turn.allow_build

    @property
    def start_turn(self):
        return self.turn.turn_state.start_turn

    @property
    def discard(self):
        return self.turn.discard


def turn_action(method):
    """Apply common logic to actions with this decorator."""
    def method_wrapper(*args, **kwargs):
        self = args[0]

        try:
            result = method(*args, **kwargs)
        except Exception:
            raise
        else:
            self.next()()
            return result

    return method_wrapper


@singleton
class UCPlayerTurn:

    def __init__(self, game: Epydemic):

        @singleton
        class TurnStates:
            def __init__(self, _self=self):
                print(f"TurnStates@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@:{_self}")
                self._self = _self
                self.turns_stk = [self.player_action] * 4 + [
                    self.buy_cards, self.epidemy, self.discard_extras, self.infection]
                self.turns = [self.player_action] * 8 + self.turns_stk[:]
                self.turn = None
                self.can_travel = True

            def start_turn(self):
                print(f"start_turn:{len(self.turns)}")
                self.turns = self.turns_stk[:]
                self.turn = self.turns.pop(0)
                # self.can_travel = True

            def next(self):
                print(f"action(next):{len(self.turns)}")
                self.turn = self.turns.pop(0)
                return self.turn

            def player_action(self):
                print(f"player_action:{len(self.turns)}")
                return self.next

            def buy_cards(self):
                _self = self._self
                _self.cards, _self.cards_in_hand = _self.cards[2:], _self.cards_in_hand + _self.cards[:2]
                self.can_travel = len(self._self.cards_in_hand) <= 7
                if CONST.EPIDEMY in _self.cards:
                    _self.cards.pop(CONST.EPIDEMY)
                    _self.infection_turn.epidemy()
                self.start_turn()
                # return self.next()()()()
                self._self.infection_turn.turn_on()
                print(f"action(buy_cards):{len(_self.cards)} in hand:{len(_self.cards_in_hand)}")
                return self.next

            def epidemy(self):
                _self = self._self
                print(f"action(epidemy) CONST.EPIDEMY in _self.cards:{CONST.EPIDEMY in _self.cards}")
                return self.next

            def discard_extras(self):
                print(f"action(discard_extras) len(self._self.cards) <= 7:{len(self._self.cards_in_hand) <= 7}")
                self.can_travel = len(self._self.cards_in_hand) <= 7
                return self.next

            def infection(self):
                pass
                self.start_turn()
                print(f"action(infection):{len(self.turns)} in hand:{len(self._self.cards_in_hand)}")

        self.infection_turn = None
        # self.can_travel = True
        self.game, self.rate = game, 2
        self.stations = []
        self.cards_in_hand = []
        self.cards = []
        self.city = "atlanta"
        self.turn_player = "dispatcher"
        self.cards = None
        self.turn_state = TurnStates()
        self.turn_state.next()
        self.cities = {}
        self.has_epidemy = False
        self.cure_need = 5

    @property
    def next(self):
        return self.turn_state.next

    def create_cities(self):
        citymap = CM
        self.cities = {city_name: City(game=self, **kwargs)
                       for city_name, kwargs in citymap.items()}
        self.cards = list(self.cities.keys())
        from random import shuffle
        shuffle(self.cards)
        self.cards, self.cards_in_hand = self.cards[7:], self.cards[:7]
        self.infection_turn = UCInfectionTurn(game=self.game)
        # self.infection_turn.initial_infection()
        # self.turn_state.start_turn()

    def current_city(self):
        return self.cities[self.city]

    def destinations(self):
        _all_destinations = list(self.cities.keys()) if self.city in self.cards_in_hand else []
        _shuttle_destinations = [
            _city for _city in self.stations if _city != self.city] if self.city in self.stations else []
        _destination = [local for local in set(
            self.cards_in_hand + self.cities[self.city].paths + _all_destinations + _shuttle_destinations
        ) if local != self.city] if self.turn_state.can_travel else []
        return [self.cities[card] for card in _destination] if self.turn_state.can_travel else []

    def is_shuttle_flight(self, city):
        flight = {city, self.city}
        # print(f"is_shuttle_flight o:{city} d:{self.city} od:{flight} st:{set(self.stations)}")
        return flight <= set(self.stations)

    def allow_build(self, allow, refuse):
        allow() if (self.city in self.cards_in_hand)\
                   and (self.city not in self.stations) and self.turn_state.can_travel else refuse()

    @turn_action
    def travel(self, city):
        city_name = city.name
        self.discard(city_name) \
            if (city_name not in self.cities[self.city].paths) and (not self.is_shuttle_flight(city)) else None
        self.city = city.name
        return city.arrive(self.turn_player)

    @turn_action
    def cure(self):
        kind, cure_payment = self.has_cure() if self.has_cure() else (None, [])
        print(f"cure_payment {cure_payment}")
        [self.cards_in_hand.remove(card) for card in cure_payment]
        return self.infection_turn.cure(kind)

    def has_cure(self):
        hand = self.cards_in_hand
        cure_payment = {
            kind: [card for card in hand if kind in self.cities[card].color] for kind in CONST.DISEASES}
        cure = [(kind, cards[:self.cure_need]) for kind, cards in cure_payment.items()
                if (len(cards) >= self.cure_need) and kind not in self.game.infection_turn.cured]
        return cure[0] if cure and self.turn_state.can_travel else False

    @turn_action
    def build(self, city):
        self.stations.append(city)
        city:City = self.cities[city]
        city.build(len(self.stations))
        # print("self.stations", self.stations)
        return len(self.stations)

    def treat(self, city, disease=None, marker="1"):
        return self._treat(city, disease, marker) if (city == self.city) and self.turn_state.can_travel else []

    @turn_action
    def _treat(self, city, disease=None, marker="1"):
        # self.infection_turn.treat(city, disease, marker)
        # disease = disease or self.game.cities[city].color
        cured = disease in self.game.cured()
        print(f"treat(self, city, disease=None, marker {city, disease, marker, cured}")
        # return self.game.cities[city].treat(disease, marker if not cured else None)
        return self.game.infection_turn.treat(city, disease, marker if not cured else None)

    def discard(self, city_name=None, hand_order=0):
        city_discard = city_name if city_name in self.cards_in_hand\
            else (self.city if hand_order == 0 else self.cards_in_hand[hand_order-1])
        print(f"self.cards_in_hand[hand_order-1] {city_discard}") if hand_order else None
        self.cards_in_hand.remove(city_discard) \
            if city_discard in self.cards_in_hand else self.cards_in_hand.remove(self.city) \
            if (city_name != self.city) and (city_name in self.cards_in_hand) else None
        self.turn_state.can_travel = len(self.cards_in_hand) <= 7

    def epidemy(self):
        self.has_epidemy = True
        self.infection_turn.epidemy()


@singleton
class UCInfectionTurn:

    def __init__(self, game: Epydemic, turn=lambda *_: ()):
        self.epidemics = CONST.EPIDEMY_LIMIT
        self.game, self.rate = game, self.epidemics.pop()
        self.infection = {kind: list(range(CONST.INFECTION_STOCK)) for kind in CONST.DISEASES}
        self.cured = set()
        self.eradicated = set()
        self.deck = list(self.game.city_names())
        self.markers = {kind: [f"{kind}_{mark}" for mark in range(CONST.INFECTION_STOCK)] for kind in CONST.DISEASES}
        from random import shuffle
        shuffle(self.deck)
        self.discard = []
        self.no_turn = turn
        self.do_turn = turn

    def turn_on(self):
        self.do_turn = self.real_turn
        print(f"turn_on do_turn {self.do_turn}")

    def can_infect(self, city_to_infect):
        return self.game.cities[city_to_infect].color not in self.eradicated

    def real_turn(self, max_rates=0):
        self.do_turn = self.no_turn
        print(f"real_turn self.rate{self.rate}")
        infection_rates = [rate for _ in range(3) for rate in range(1, max_rates)] if max_rates else [1]*self.rate
        city_to_infect = self.deck.pop(0)
        infection_rates = infection_rates if self.can_infect(city_to_infect) else []
        return [self.infect(city_to_infect, count=count) for count in infection_rates]

    def treat(self, city, disease, marker):
        markers = self.game.cities[city].treat(disease, marker)
        print(f">>>>>>>>>turn treat{markers}")
        [self.markers[disease].append(mark) for mark in markers]
        disease_clean = len(self.markers[disease]) == CONST.INFECTION_STOCK
        self.eradicated.add(disease) if (disease in self.cured) and disease_clean else None
        return markers

    def turn(self, rate=0):
        print(f"turn do_turn{self.do_turn}")
        return self.do_turn(rate)

    def infect(self, city, disease=None, count=1):
        self.discard.append(city)
        disease = disease or self.game.cities[city].color
        return self.game.cities[city].infect(self.markers, disease, count=count)

    def cure(self, disease):
        self.cured.add(disease) if disease else None
        print(f"turn  cured.add{self.cured} {set(CONST.DISEASES)}")
        disease_clean = len(self.markers[disease]) == CONST.INFECTION_STOCK
        self.eradicated.add(disease) if (disease in self.cured) and disease_clean else None
        return self.cured == set(CONST.DISEASES)

    def initial_infection(self):
        infection = []
        [infection.append(self.epidemy(count, False)) for _ in (3, 2, 1) for count in (3, 2, 1)]
        return infection

    def epidemy(self, count=3, rate=True):
        city = self.deck.pop()
        self.rate = self.epidemics.pop() if rate else self.rate
        return self.infect(city, count=count)


class City(object):
    """
    The City class
    * contains the name and color of the City
    * manages the cube state
    * manages infections and outbreaks.
    No city graph data or player data is in this class.
    """

    def __init__(self, game, name, paths=(), color="blue", x=0, y=0, **_):
        self.x, self.y = float(x), float(y)
        self.player = None
        self.game = game
        self.name = name
        self.infections = []
        self.color = color
        self.paths = paths
        self.cubes = {"blue": [],
                      "yellow": [],
                      "black": [],
                      "red": []}
        self.has_research_station = False

    def __repr__(self):
        return ": ".join([self.name, ", ".join(self.paths)])

    def arrive(self, player):
        self.player = player
        return self.x, self.y

    def build(self, station):
        self.has_research_station = station
        return self.x, self.y

    def treat(self, disease, marker):
        markers = [marker] if marker else list(self.cubes[disease])
        print(f"def treat(self, disease, marker): city:{self.name} markers:{markers} diseases {self.cubes[disease]}")
        return [self.cubes[disease].remove(mark) or mark for mark in markers]

    def infect(self, markers, disease=None, outbrake=lambda: None, count=1):
        new_diseases = count - len(self.cubes[disease])
        outbrake() if new_diseases > 3 else None
        for _ in range(min(3, new_diseases)):
            marker = markers[disease]
            marker = marker.pop(0)  # marker.keys(0))
            disease = disease or self.color
            self.cubes[disease].append(marker)
        return self.x, self.y, self.name, self.cubes


class Util:

    @staticmethod
    def _labels():
        from epydemic.citymap import CM
        text = '''<text x="{x}" y="{y}" fill="red" style="stroke-width:0.36;
        font-size:3.2px;font-family:'Liberation Sans Narrow';
        font-weight:bold;font-style:normal;font-stretch:condensed;
        font-variant:normal;fill:#ffffff">{name}</text>'''
        d, s = 4.23, 1
        citymap = CM
        c = {city_name: City(game=None, **kwargs) for city_name, kwargs in citymap.items()}
        labels = [text.format(
            x=cy.x - d * (len(cy.name) / (16 if len(cy.name) > 6 else 128)), y=cy.y + 2.6 * d,
            name=cy.name.upper().replace("_", " "))
            for cy in c.values()]
        [print(label) for label in labels]

    @staticmethod
    def _routes():
        from epydemic.citymap import CM
        line = '<line x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}" style="stroke:rgb(100,170,200);stroke-width:1" />'
        d, s = 4.23, 1
        citymap = CM
        c = {city_name: City(game=None, **kwargs) for city_name, kwargs in citymap.items()}
        routes = [(c[a], c[b]) for a, b in set([tuple(sorted([ak, b])) for ak, av in c.items() for b in av.paths])]
        routes = [line.format(x1=a.x + d, y1=a.y + d, x2=b.x + d, y2=b.y + d) for a, b in routes]
        [print(route) for route in routes]

    @staticmethod
    def _init():
        cities = "/home/carlo/Downloads/agile/pydemic-master/city_map_adj_list_data.txt"
        citimap = "/home/carlo/Documentos/dev/epydemic/src/epydemic/citymap.py"
        citisvg = "/home/carlo/Documentos/dev/epydemic/src/epydemic/images/epydemic.svg"
        cityimg = "disease_large_"
        # cityimg = "epydemic/images/disease_large_"
        citytext = CONST.HEAD + 'CM = """{}"""'
        with open(cities, "r") as _cities, open(citimap, "w") as _citymap, open(citisvg, "r") as _citylocs:
            _cities = _cities.read().split("\n")
            # _citlocs = _citylocs.read()
            _citlocs = '[{}]'.format(", ".join(["{{{}}}".format(
                ", ".join([
                    arg.replace("=", '":').replace("       ", '"').replace(cityimg, '').replace(".png", '')
                    for arg in city.split(" />")[0].replace("xlink:href", "color").split("\n") if arg]))
                for city in _citylocs.read().split("<image")[2:]]))
            from json import loads
            _citlocs = {arg['id'][2:]: {k: v for k, v in arg.items() if k in "color x y height width".split()}
                        for arg in loads(_citlocs)}
            print(_citlocs)
            _cities = {
                city.split()[0]: dict(name=city.split()[0], paths=city.split()[1:], **(_citlocs[city.split()[0][:4]]))
                for city in _cities if city}
            print(_cities)
            from json import dumps
            _citymap.write(citytext.format(dumps(_cities, indent=4)))


class CONST:
    EPIDEMY_LIMIT = [4, 4, 3, 3, 2, 2, 2]
    EPIDEMY = "epidemic"
    WIN = "epydemic/images/fim.gif"
    PLAYER_PNG = "epydemic/images/{}.png"
    ERADICATED_PNG = "epydemic/images/{}_eradicated.png"
    CITY_BUILD_ = "_city_build_"
    CITY_SELECTION_ = "_city_selection_"
    CITY_CURE_ = "_city_cure_{}_"
    DISEASES = "black red yellow blue".split()
    VIRUS = "epydemic/images/cube_{}.png"
    INFECTION_STOCK = 40
    CITY_SELECTION = "epydemic/images/city_sel.png"
    STATION_PNG = "epydemic/images/station.png"
    CURE_PNG = "epydemic/images/{}_cured.png"
    ATLANTA = "atlanta"
    LAYER_ = "layer1"
    CROSSHAIR_PNG = "epydemic/images/crosshair.png"
    Info = namedtuple("Responses", "card move build player infect cure")
    INFO = Info(card=None, move=None,  build=None,  player=None,  infect=None,  cure=None)
    # INFO = Info(None, None, None, None, None, None)
    HEAD = """# -*- coding: UTF8 -*-
    # This file is part of  program Epydemic
    # Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
    # `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
    # SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception
    """
    CARDNUM = ['san_francisco', 'chicago', 'atlanta', 'montreal', 'washington', 'new_york', 'madrid', 'london', 'paris',
               'essen', 'milan', 'st_petersburg', 'los_angeles', 'mexico_city', 'lima', 'santiago', 'bogota', 'miami',
               'buenos_aires', 'sao_paolo', 'lagos', 'kinshasa', 'johannesburg', 'khartoum', 'algiers', 'cairo',
               'istanbul',
               'baghdad', 'riyadh', 'moscow', 'tehran', 'karachi', 'mumbai', 'delhi', 'chennai', 'kolkata', 'jakarta',
               'bangkok', 'beijing', 'shanghai', 'hong_kong', 'ho_chi_minh_city', 'seoul', 'taipei', 'manila',
               'tokyo', 'osaka', 'sydney']


def main(brython):
    epydemic = Epydemic(brython)
    return epydemic


if __name__ == "__main__":
    def offline_main():
        from unittest.mock import MagicMock

        class Browser:
            mock = MagicMock()
            document, alert, html, window, ajax, timer, svg = [mock] * 7

        epydemic = main(Browser)
        _ = epydemic.render().cities
        epydemic.build(CONST.ATLANTA)
        prev_cards = epydemic.cards_in_hand()[:]
        epydemic.turn.cure_need = 3
        print(f"city {epydemic.current_city().name} "
              f"station {epydemic.current_city().has_research_station} card {prev_cards}")
        epydemic.infect(3)
        epydemic.cure()
        new_cards = epydemic.cards_in_hand()
        print(f"has cure {epydemic.can_cure()} cards {set(prev_cards) - set(new_cards)}")
    offline_main()
