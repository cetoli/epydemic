# spyms


# A super-simple Superpython IDE with microservices" that exposes port 8080
#
# VERSION               0.1.0
FROM python:3.8-slim-buster
MAINTAINER Carlo Oliveira <cetoli@gmail.com>

# create user
RUN groupadd web
RUN useradd -d /home/bottle -m bottle

# make sure sources are up to date
# RUN echo "deb http://archive.ubuntu.com/ubuntu precise main universe" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get upgrade -y

# install pip and hello-world server requirements
RUN apt-get install python3-pip -y
ADD src /home/bottle/src
WORKDIR /home/bottle/src
# RUN pip3 install bottle
RUN apt-get install python3-bottle -y

# in case you'd prefer to use links, expose the port
EXPOSE 8080
ENTRYPOINT ["/usr/bin/python3", "/home/bottle/src/sppy/wsgi.py"]
USER bottle
ARG port=80
ENV PORT=$port
# Run the app.  CMD is required to run on Heroku
# $PORT is set by Heroku
CMD gunicorn --bind 0.0.0.0:$PORT wsgi
